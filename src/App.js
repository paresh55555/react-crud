import React, { Component } from 'react';
import './dummy.css';
import { Button } from 'reactstrap';
import { Table } from 'reactstrap';
import '@fortawesome/react-fontawesome';
import 'react-fontawesome';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
        id: Date.now(),
        username: "",
        email:"",
        number: "",
        items:[],
        inputtext: [],
        sub: true,
    };
      this.showRight = this.showRight.bind(this);
  }
    showRight = () => {
        this.setState({ rightMenu: !this.state.rightMenu })
    };

    handleChange =(e)=>{
      const {name, value} = e.target;

      this.setState({
          [name] : value,
      });

      // let items = [...this.state.items];
      // items.push({username:this.state.username,email: this.state.email, number: this.state.number});
  };

    addWithValidation = ()=> {
        if(this.state.sub === true){
            if(this.state.username === '' ){
                alert('Enter the username');
                return false;
            }if(this.state.email === ''){
                alert('Enter the email ID');
                return false;
            }if(this.state.number === ''){
                alert('Enter the number');
                return false;
            }else{
                const { items } = this.state;
                items.push({id:Date.now()+1, username:this.state.username,email: this.state.email, number: this.state.number});
                this.setState({
                    items:items,
                });
            }
            this.setState({
                username : '',
                email : '',
                number : '',

            });
        }else {
            const { items } = this.state;
            let upd = {username:this.state.username,email: this.state.email, number: this.state.number};
            // let index = items.findIndex((i)=>i.username === upd.username )
            let index = items.findIndex((i)=>i.id === upd.id);
            items.splice(index,1,upd);
            this.setState({
                items,
                username : '',
                email : '',
                number : '',
                sub:true,
            });
        }
    };
    updatefunc = () => {

        const { items } = this.state;
        let upd = {username:this.state.username,email: this.state.email, number: this.state.number};
        // let index = items.findIndex((i)=>i.username === upd.username )
        let index = items.findIndex((i)=>i.id === upd.id) ;
        items.splice(index,1,upd);
        this.setState({
            items,
            username : '',
            email : '',
            number : '',
            sub:true,
        });


        // items.filter((item,event) => {
        //
        //     if (item !== -1) {
        //         console.log(this.state.number);
        //         console.log(this.state.email);
        //         console.log(this.state.username);
        //     }else {
        //         alert('not update');
        //     }
        // });
    };

    deletefun = (ed) => {
        // const trget = e.target.value;
        let confirmOk = window.confirm('Are you sure want to delete...');
        if(confirmOk === true){
            let del = this.state.items ;
            del.splice(this.state.items,1);
            this.setState({
                items:del
            });
        }else {
            return false;
        }
    };

    editinfo =(data) =>{
       let  datae = this.state.items;
        console.log('edit data....',datae[data]);
        this.setState({
            username: data.username,
            email : data.email,
            number : data.number,
            sub :  false,
        });
            console.log(this.setState.username);
    };
    inval = () => {
        this.state.inputtext.map((text,index)=>{
            return(
                <div key={index}>
                    <input value={text}/>
                </div>
            )
        })
    }
    textfunc = (dataq) => {
        this.setState({
            inputtext:[...this.state.inputtext, dataq],

        });
    };

        render() {
            const {items,username,email,number}= this.state;
            return (
                <div>
                    <div className ='box'>
                        <h1 className='title center'>Add Data</h1>&nbsp;
                        <input type="text" name='id' onChange={(e)=>{this.handleChange(e)}}  className= 'form-control none'  /> <br/>
                        <label ><i className="fa fa-search"></i>User Name</label>

                        <input type="text" name='username' onChange={(e)=>{this.handleChange(e)}} value={username} className= 'form-control'  /> <br/>
                        <label>Email ID</label>
                        <input type="email" name='email' onChange={(e)=>{this.handleChange(e)}} value={email} className='form-control'/><br/>
                        <label >Contact No.</label>
                        <input type="text" name= 'number'  onChange={(e)=>{this.handleChange(e)}} value={number} className='form-control'/><br/>
                        <Button color='primary' name='sub' onChange={(e)=>{this.handleChange(e)}}  onClick={this.addWithValidation} >submit</Button>
                        <button color='primary' className='btn btn-danger'onClick={() => this.updatefunc}>Update
                        </button>

                    </div>

                    <div className='tableBox'>
                        <h2 className='center'>Data Table</h2>
                        <Table bordered striped  className ='tableData'>
                            <thead>

                                <th>username</th>
                                <th>email</th>
                                <th>contact</th>
                                <th>Action</th>
                            </thead>
                            <p>A pdddf to my <a href="/../../ntweorkweb/qa.pdf">my pdf pddf brief</a>.</p>

                            <tbody>

                            {items.map((d1,i) =>{
                                return(
                                    <tr>

                                        <td>{d1.username}</td>
                                        <td>{d1.email}</td>
                                        <td>{d1.number}</td>
                                        <td>
                                            <button color='primary' className='btn btn-danger' onClick={() => this.deletefun(d1)}>Delete</button>&nbsp;
                                            <button color='primary' className='btn btn-info' onClick={() => this.editinfo(d1)}>Edit</button>&nbsp;
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </Table>
                    </div>

                </div>
            );
        }
}
// class JTP extends React.Component {
//   render() {
//     return (
//         <div className ='box'>
//           <h1 className='title center'>log in</h1>&nbsp;
//           <label >USER NAME</label>
//           <input   type="text" name='username' onChange={(e)=>{}} className= 'form-control'  /> <br/>
//           <label >Password</label>
//           <input type="password" className='form-control' name="password" value={this.props.pass}/><br/>
//           <button className= 'btn btn-primary' onClick={this.addWithValidation} >submit</button>
//         </div>
//     );
//   }
// }
export default App;